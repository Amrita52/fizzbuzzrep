﻿namespace BusinessLogic
{
    public class NonDivisibilityCheck : IGetMessages
    {
        public bool IsDivisible(int number)
        {
            return number % 3 != 0 && number % 5 != 0 ? true : false;
        }

        public string GetMessages(int number)
        {
            return (IsDivisible(number)) ? number.ToString() : null;

        }

    }

}