﻿using System;
using NUnit.Framework;
using BusinessLogic;
using Moq;

namespace BusinessLogicTest
{
    [TestFixture]
    public class DivisibilityCheckByThreeTests
    {
        IGetMessages divisibleOperation;
        Mock<ISpecifiedDayCheck> mock = new Mock<ISpecifiedDayCheck>();

        [SetUp]
        public void Setup()
        {
            divisibleOperation = new DivisibilityCheckByThree(mock.Object);
        }

        [Test]
        public void DivisibilityCheckByThreeTests_DivisibilityCheckByThree_IsDivisible_ShouldReturnTrue()
        {
            //Act
            var result = divisibleOperation.IsDivisible(9);

            //Act
            Assert.AreEqual(result, true);
        }

        [Test]
        public void DivisibilityCheckByThreeTests_DivisibilityCheckByThree_IsDivisible_ShouldReturnFalse()
        {
            //Act
            var result = divisibleOperation.IsDivisible(5);

            //Act
            Assert.AreEqual(result, false);
        }

        [Test]
        public void DivisibilityCheckByThreeTests_DivisibilityCheckByThree_IsDivisible_ShouldReturnFalse_case2()
        {
            //Act
            var result = divisibleOperation.IsDivisible(15);

            //Act
            Assert.AreEqual(result, false);
        }

        [Test]
        public void DivisibilityCheckByThreeTests_DivisibilityCheckByThree_ShouldReturnWizz_WhenDivisibleByThree_And_SpecifiedDayCheckIsTrue()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleOperation.GetMessages(9);

            //Assert
            Assert.AreEqual(result, "Wizz");
        }

        [Test]
        public void DivisibilityCheckByThreeTests_DivisibilityCheckByThree_ShouldReturnNull_WhenNotDivisibleByThree_And_SpecifiedDayCheckIsTrue()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleOperation.GetMessages(10);

            //Assert
            Assert.AreEqual(result, null);
        }

        [Test]
        public void DivisibilityCheckByThreeTests_DivisibilityCheckByThree_ShouldReturnFizz_WhenDivisibleByThree_And_SpecifiedDayCheckIsFalse()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleOperation.GetMessages(9);

            //Assert
            Assert.AreEqual(result, "Fizz");
        }

        [Test]
        public void DivisibilityCheckByThreeTests_DivisibilityCheckByThree_ShouldReturnNull_WhenNotDivisibleByThree_And_SpecifiedDayCheckIsFalse()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleOperation.GetMessages(10);

            //Assert
            Assert.AreEqual(result, null);
        }


    }

}

