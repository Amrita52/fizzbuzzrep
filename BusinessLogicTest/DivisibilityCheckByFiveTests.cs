﻿using System;

namespace BusinessLogic
{
    public class DivisibilityCheckByFiveTests : IGetMessages
    {
        private ISpecifiedDayCheck specifiedDay;

        public DivisibilityCheckByFiveTests(ISpecifiedDayCheck specifiedDay)
        {
            this.specifiedDay = specifiedDay;
        }

        public bool IsDivisible(int number)
        {
            return number % 5 == 0 && number % 3 != 0 ? true : false;
        }

        public string GetMessages(int number)
        {
            if (IsDivisible(number))
            {
                return specifiedDay.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek) ? "Wuzz" : "Buzz";
            }
            else
            {
                return null;
            }

        }

    }
}

