﻿using System.ComponentModel.DataAnnotations;
using PagedList;

namespace fizzbuzzWeb.Models
{
    public class FizzBuzzParameters
    {
        [Range(1, 1000, ErrorMessage = "Number should be between 1 to 1000")]
        public int InputNumber { get; set; }
        public IPagedList<string> OutputList { get; set; }
    }
}