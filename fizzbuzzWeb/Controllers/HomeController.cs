﻿using BusinessLogic;
using System.Web.Mvc;
using PagedList;
using fizzbuzzWeb.Models;

namespace fizzbuzzWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDivisibilityRule divisibility;

        public HomeController(IDivisibilityRule divisibility)
        {
            this.divisibility = divisibility;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new FizzBuzzParameters());
        }


        [HttpGet]
        public ActionResult DisplayResult(FizzBuzzParameters inputsFromUser, int? i)
        {
            if (ModelState.IsValid)
            {
                inputsFromUser.OutputList = this.divisibility.DivisibilityRule(inputsFromUser.InputNumber).ToPagedList(i ?? 1, 20);
                return View("DisplayResult", inputsFromUser);
            }
            else
            {
                return View("Index", inputsFromUser);
            }

        }

    }

}