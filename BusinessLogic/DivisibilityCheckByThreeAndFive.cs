﻿using System;

namespace BusinessLogic
{
    public class DivisibilityCheckByThreeAndFive : IGetMessages
    {
        private ISpecifiedDayCheck specifiedDay;

        public DivisibilityCheckByThreeAndFive(ISpecifiedDayCheck specifiedDay)
        {
            this.specifiedDay = specifiedDay;
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0 && number % 5 == 0 ? true : false;
        }

        public string GetMessages(int number)
        {
            if (IsDivisible(number))
            {
                return specifiedDay.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek) ? "WizzWuzz" : "Fizz Buzz";
            }
            else
            {
                return null;
            }

        }

    }

}
