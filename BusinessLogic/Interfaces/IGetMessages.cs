﻿namespace BusinessLogic
{
    public interface IGetMessages
    {
        bool IsDivisible(int input);
        string GetMessages(int input);
    }

}