﻿using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic
{
    public class GetMessages : IDivisibilityRule
    {
        private IList<IGetMessages> getMessages;

        public GetMessages(IList<IGetMessages> getMessages)
        {
            this.getMessages = getMessages;
        }

        public IList<string> DivisibilityRule(int input)
        {
            var result = new List<string>();

            for (int index = 1; index <= input; index++)
            {
                var items = this.getMessages.FirstOrDefault(x => x.IsDivisible(index));
                result.Add(items.GetMessages(index));
            }

            return result;
        }

    }

}