﻿using System;


namespace BusinessLogic
{
    public class DivisibilityCheckByThree : IGetMessages
    {
        private ISpecifiedDayCheck specifiedDay;

        public DivisibilityCheckByThree(ISpecifiedDayCheck specifiedDay)
        {
            this.specifiedDay = specifiedDay;
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0 && number % 5 != 0 ? true : false;
        }

        public string GetMessages(int number)
        {
            if (IsDivisible(number))
            {
                return specifiedDay.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek) ? "Wizz" : "Fizz";
            }
            else
            {
                return null;
            }

        }

    }

}
