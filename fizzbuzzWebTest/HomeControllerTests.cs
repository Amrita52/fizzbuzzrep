﻿using System.Collections.Generic;
using System.Web.Mvc;
using NUnit.Framework;
using BusinessLogic;
using Moq;
using fizzbuzzWeb.Controllers;
using fizzbuzzWeb.Models;


namespace fizzbuzzWebTest
{
    [TestFixture]
    public class HomeControllerTests
    {
        private HomeController homeController;
        private Mock<IDivisibilityRule> mock;
        public FizzBuzzParameters input;
        List<string> outputList = new List<string>(new string[] { "1", "2", "Fizz", "4", "Buzz" });

        [SetUp]
        public void GeneralSetup()
        {
            mock = new Mock<IDivisibilityRule>();
            mock.Setup(x => x.DivisibilityRule(It.IsAny<int>())).Returns(outputList);
            homeController = new HomeController(mock.Object);
        }

        [Test]
        public void HomeControllerTests_Homecontroller_ShouldReturnView()
        {
            //Act
            input = new FizzBuzzParameters();
            input.InputNumber = -5;
            homeController.ModelState.AddModelError("Range", "number should be between 1 to 1000");
            var output = homeController.DisplayResult(new FizzBuzzParameters(), 1) as ViewResult;

            //Assert
            Assert.AreEqual(output.ViewName, "Index");
        }

        [Test]
        public void HomeControllerTests_Homecontroller_ShouldReturnDisplayResultView()
        {
            //Act
            input = new FizzBuzzParameters();
            input.InputNumber = 5;
            var output = homeController.DisplayResult(input, 1) as ViewResult;

            //Assert
            Assert.AreEqual(output.ViewName, "DisplayResult");
            Assert.IsNotNull(input.OutputList);
        }
    }

}